/***************************************************************************
 *   Copyright (C) 2018 by pilar   *
 *   pilarb@unex.es   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "imageprocess.h"
#include <cmath>
#include <iostream>


void imageprocess::rotar(uchar * imgO, uchar * imgD, float angle)
{
    short cw;
    int sin1000, cos1000;

    sin1000 = sin(angle)*1000.;
    cos1000 = cos(angle)*1000.;

//    uchar * dirOrig = imgO;
//    uchar * dirDest = imgD;
//    int fO, cO, indiceO;
//    for(int fD=0; fD<800; fD++)
//    {
//        for(int cD=0; cD<800; cD++)
//        {
//            fO = (sin1000*(cD-400) + cos1000*(fD-400))/1000;
//            fO = fO + 240;
//            cO = (cos1000*(cD-400) - sin1000*(fD-400))/1000;
//            cO = cO + 320;
//            if(fO>=0 && fO<480 && cO>=0 && cO <640)
//            {
//                indiceO = fO*640+cO;
//                *dirDest=*(dirOrig+indiceO);
//            }
//            else
//                *dirDest=0;
//            dirDest++;
//        }
//    }

    asm volatile(
                "\n\t"
                "mov %0,%%rsi\n\t"//imgO
                "mov %1,%%rdi\n\t"//imgD
                "movswq %2,%%r8\n\t"//sin1000
                "movswq %3,%%r9\n\t"//cos1000

                "mov $0,%%rcx\n\t"
                "fDfor:\n\t"
                        "cmp $800,%%rcx\n\t"
                        "jge finfDfor\n\t"
                        "mov $0,%%rbx\n\t"
                        "cDfor:\n\t"
                                "cmp $800,%%rbx\n\t"
                                "jge fincDfor\n\t"

                                "mov %%rbx,%%rax\n\t"
                                "sub $400,%%rax\n\t"
                                "imul %%r8\n\t"
                                "mov %%rax,%%r10\n\t"
                                "mov %%rcx,%%rax\n\t"
                                "sub $400,%%rax\n\t"
                                "imul %%r9\n\t"
                                "add %%rax,%%r10\n\t"
                                "mov %%r10,%%rax\n\t"


                "mov $0,%%rdx;"
                                "cqto\n\t"

                                "mov $1000,%%r15\n\t"
                                "idiv %%r15\n\t"
                                "mov %%rax,%%r10\n\t"
                                "add $240,%%r10\n\t"

                                "mov %%rbx,%%rax\n\t"
                                "sub $400,%%rax\n\t"
                                "imul %%r9\n\t"
                                "mov %%rax,%%r11\n\t"
                                "mov %%rcx,%%rax\n\t"
                                "sub $400,%%rax\n\t"
                                "imul %%r8\n\t"
                                "sub %%rax,%%r11\n\t"
                                "mov %%r11,%%rax\n\t"


                "mov $0,%%rdx;"
                                "cqto;"

                                "mov $1000,%%r15\n\t"
                                "idiv %%r15\n\t"
                                "mov %%rax,%%r11\n\t"
                                "add $320,%%r11\n\t"

                                "cmp $0,%%r10\n\t"
                                "jl relse\n\t"
                                "cmp $480,%%r10\n\t"
                                "jge relse\n\t"
                                "cmp $0,%%r11\n\t"
                                "jl relse\n\t"
                                "cmp $640,%%r11\n\t"
                                "jge relse\n\t"
                                        "mov %%r10,%%rax\n\t"
                                        "mov $640,%%rdx\n\t"
                                        "imul %%rdx\n\t"
                                        "add %%r11,%%rax\n\t"
                                        "mov %%rax,%%r12\n\t"
                                        "add %%rsi,%%rax\n\t"
                                        "movb (%%rax),%%al\n\t"
                                        "movb %%al,(%%rdi)\n\t"
                                        "jmp rfinif\n\t"
                                "relse:\n\t"
                                        "mov $0,%%rax\n\t"
                                        "movb %%al,(%%rdi)\n\t"
                                "rfinif:\n\t"
                                "inc %%rdi\n\t"
                                "inc %%rbx\n\t"
                                "jmp cDfor\n\t"
                        "fincDfor:\n\t"
                        "inc %%rcx\n\t"
                        "jmp fDfor\n\t"
                "finfDfor:\n\t"

                :
                :"m"(imgO),"m"(imgD),"m"(sin1000),"m"(cos1000)
                :"memory"
                );

}


void imageprocess::zoom(uchar * imgO, uchar * imgD, float s, int dx, int dy)
{
    short cw;
    int sInt;
    int ampliar;


    if(s>=1)
    {
        sInt = s;
        ampliar = 1;
    }
    else
    {
        sInt = rint(1./s);
        ampliar = 0;
    }

//    uchar * dirOrig = imgO;
//    uchar * dirDest = imgD;
//    int fO,cO,indiceO;
//    for(int fD=0; fD<480; fD++)
//    {
//        if (ampliar)
//            fO = (fD+dy)/sInt;<<
//        else
//            fO = (fD+dy)*sInt;
//        for(int cD=0; cD<640; cD++)
//        {
//            if(ampliar)
//                cO = (cD+dx)/sInt;
//            else
//                cO = (cD+dx)*sInt;
//            if(fO>=0 && fO<800 && cO>=0 && cO <800)
//            {
//                indiceO = fO*800+cO;
//                *dirDest=*(dirOrig+indiceO);
//            }
//            else
//                *dirDest=0;
//            dirDest++;
//        }
//    }

    //cw es short

    float one = 1;
    float memory_dump=0;

    int cD,fD;
    uchar * dirOrig =imgO;
    uchar * dirDest =imgD;

    asm (
        "fstcw %5\n\t"//cw, guarda en cw
        "fwait\n\t"//
        "mov %5, %%ax\n\t"
        "and $0xf3ff, %%ax\n\t"
        "or $0x0c00, %%ax\n\t"
        "push %%rax\n\t"
        "fldcw (%%rsp)\n\t"
        "pop %%rax\n\t"

        //Insertar aquí el código del procedimiento

    "mov %0,%%rsi\n\t"
    "mov %1,%%rdi\n\t"



        "movq    %0, %%rax\n\t"
        "movq    %%rax,  %13\n\t"
        "movq    %1, %%rax\n\t"
        "movq    %%rax,  %12\n\t"
        "movl    $0,  %11\n\t"
        "for1:\n\t"
                "cmpl    $479,  %11\n\t"
                "jg      finfor1\n\t"
                "cmpl    $0,  %7\n\t"
                "je      else2\n\t"
                        "movl     %11, %%edx\n\t"
                        "movl    %4, %%eax\n\t"
                        "addl    %%edx, %%eax\n\t"
                        "cltd\n\t"
                        "idivl    %6\n\t"
                        "movl    %%eax, %%r8d\n\t"
                        "jmp     finif2\n\t"
                "else2:\n\t"
                        "movl     %11, %%edx\n\t"
                        "movl    %4, %%eax\n\t"
                        "addl    %%eax, %%edx\n\t"
                        "movl     %6, %%eax\n\t"
                        "imull   %%edx, %%eax\n\t"
                        "movl    %%eax, %%r8d\n\t"
                "finif2:\n\t"
                "movl    $0,  %10\n\t"
                "for2:\n\t"
                        "cmpl    $639,  %10\n\t"
                        "jg      finfor2\n\t"
                        "cmpl    $0,  %7\n\t"
                        "je      else3\n\t"
                                "movl     %10, %%edx\n\t"
                                "movl   	%3, %%eax\n\t"
                                "addl    %%edx, %%eax\n\t"
                                "cltd\n\t"
                                "idivl    %6\n\t"
                                "movl    %%eax, %%r9d\n\t"
                                "jmp     finif3\n\t"
                        "else3:\n\t"
                                "movl     %10, %%edx\n\t"
                                "movl    %3, %%eax\n\t"
                                "addl    %%eax, %%edx\n\t"
                                "movl     %6, %%eax\n\t"
                                "imull   %%edx, %%eax\n\t"
                                "movl    %%eax, %%r9d\n\t"
                        "finif3:\n\t"
                        "cmpl    $0, %%r8d\n\t"
                        "js      else4\n\t"
                        "cmpl    $799, %%r8d\n\t"
                        "jg      else4\n\t"
                        "cmpl    $0, %%r9d\n\t"
                        "js      else4\n\t"
                        "cmpl    $799, %%r9d\n\t"
                        "jg      else4\n\t"
                                "movl    %%r8d, %%eax\n\t"
                                "imull   $800, %%eax, %%edx\n\t"
                                "movl    %%r9d, %%eax\n\t"
                                "addl    %%edx, %%eax\n\t"
                                "movl    %%eax, %%r10d\n\t"
                                "movl    %%r10d, %%eax\n\t"
                                "movslq  %%eax, %%rdx\n\t"
                                "movq     %13, %%rax\n\t"
                                "addq    %%rdx, %%rax\n\t"
                                "movzbl  (%%rax), %%edx\n\t"
                                "movq     %12, %%rax\n\t"
                                "movb    %%dl, (%%rax)\n\t"
                                "jmp     finif4\n\t"
                        "else4:\n\t"
                                "movq     %12, %%rax\n\t"
                                "movb    $0, (%%rax)\n\t"
                        "finif4:\n\t"
                        "addq    $1,  %12\n\t"
                "addl    $1,  %10\n\t"
                "jmp     for2\n\t"
                "finfor2:\n\t"
        "addl    $1,  %11\n\t"
        "jmp     for1\n\t"
        "finfor1:\n\t"


        "fldcw %5\n\t"

        :
        : "m" (imgO), "m" (imgD), "m" (s), "m" (dx), "m" (dy), "m" (cw), "m" (sInt), "m" (ampliar), "m" (one), "m"(memory_dump),
            "m"(cD),"m"(fD),"m"(dirDest),"m"(dirOrig)
        : "memory","%rax","%rbx","%rcx","%rdx","%rsi","%rdi","%r8","%r9","%r10","%r11","%r12","%r13","%r15"
    );


}



void imageprocess::volteoHorizontal(uchar * imgO, uchar * imgD)
{

//    uchar *dirOrig = imgO;
//    uchar *dirDest = imgD;
//    dirOrig = dirOrig + 639;
//    for(int f=0; f<480; f++)
//    {
//        for(int c=0; c<640; c++)
//        {
//            *dirDest=*dirOrig;
//            dirOrig--;
//            dirDest++;
//        }
//        dirOrig=dirOrig+1280;
//    }
    asm volatile(
        "\n\t"

                "mov %0,%%r8\n\t"//imgOrg
                "mov %1,%%r9\n\t"//imgDst
                "add $639,%%r8\n\t"

                "mov $0,%%rcx\n\t"
                "altVertical:\n\t"

                        "push %%rcx\n\t"
                        "mov $0,%%rcx\n\t"
                        "anchVertical:\n\t"

                                "movb (%%r8),%%al\n\t"
                                "movb %%al,(%%r9)\n\t"
                                "dec %%r8\n\t"
                                "inc %%r9\n\t"

                        "inc %%rcx\n\t"
                        "cmp $640,%%rcx\n\t"
                        "jl anchVertical\n\t"

                        "pop %%rcx\n\t"
                        "add $1280,%%r8\n\t"

                "inc %%rcx\n\t"
                "cmp $480,%%rcx\n\t"
                "jl altVertical\n\t"

        :
        : "m" (imgO),	"m" (imgD)
        : "memory","%rcx","%rax","%r8","%r9"
    );


//    asm volatile(
//        "\n\t"


//        :
//        : "m" (imgO),	"m" (imgD)
//        : "memory"
//    );

}

void imageprocess::volteoVertical(uchar * imgO, uchar * imgD)
{
/*
    uchar *dirOrig = imgO;
    uchar *dirDest = imgD;
    dirOrig = dirOrig + 479*640;
    for(int f=0; f<480; f++)
    {
        for(int c=0; c<640; c++)
        {
            *dirDest=*dirOrig;
            dirOrig++;
            dirDest++;
        }
        dirOrig=dirOrig-1280;
    }

*/


    asm volatile(
        "\n\t"

                "mov %0,%%r8\n\t"//imgOrg
                "mov %1,%%r9\n\t"//imgDst
                "add $306560,%%r8\n\t"

                "mov $0,%%rcx\n\t"
                "altHorizontal:\n\t"

                        "push %%rcx\n\t"
                        "mov $0,%%rcx\n\t"
                        "anchHorizontal:\n\t"

                                "movb (%%r8),%%al\n\t"
                                "movb %%al,(%%r9)\n\t"
                                "inc %%r8\n\t"
                                "inc %%r9\n\t"

                        "inc %%rcx\n\t"
                        "cmp $640,%%rcx\n\t"
                        "jl anchHorizontal\n\t"

                        "pop %%rcx\n\t"
                        "sub $1280,%%r8\n\t"

                "inc %%rcx\n\t"
                "cmp $480,%%rcx\n\t"
                "jl altHorizontal\n\t"

        :
        : "m" (imgO),	"m" (imgD)
        : "memory","%rcx","%rax","%r8","%r9"
    );

}


void imageprocess::iluminarLUT(uchar * tablaLUT, uchar gW)
{
    /*
    uchar * dirLUT = tablaLUT;
    for(int g=0; g<gW; g++)
        *(dirLUT+g) = (g*255)/gW;
    for(int g=gW; g<256; g++)
        *(dirLUT+g) = 255;
*/

    /*
     * dirLUT -16 r8
     * gw     -28 rbx
     * g      -4  rcx
     *
     */



    asm volatile(
                "\n\t"
                "mov %0,%%r8\n\t"
                "movzbq %1,%%rbx\n\t"
                //"dec %%rbx\n\t"

                "mov $0,%%rcx\n\t"
                "i1:\n\t"
                "cmp %%rbx,%%rcx\n\t"
                "jge ni1\n\t"

                        "mov %%rcx,%%rax\n\t"
                        "sal $8,%%rax\n\t"
                        "sub %%rcx,%%rax\n\t"//g*255
                        "cltd\n\t"
                        "div %%bx\n\t"
                        "mov %%r8,%%rdx\n\t"
                        "add %%rcx,%%rdx\n\t"
                        "movb %%al,(%%rdx)\n\t"
                "inc %%rcx\n\t"
                //"cmp %%rbx,%%rcx\n\t"
                "jmp i1\n\t"
                "ni1:\n\t"
                
                "mov %%rbx,%%rcx\n\t"
                "i2:\n\t"
                "cmp $256,%%rcx\n\t"
                "jge ni2\n\t"

                        "mov %%r8,%%rdx\n\t"
                        "add %%rcx,%%rdx\n\t"
                        "movb $255,(%%rdx)\n\t"
                        "inc %%rcx\n\t"
                //"cmp $256,%%rcx\n\t"
                "jmp i2\n\t"
                "ni2:\n\t"





        :
        : "m" (tablaLUT), "m" (gW)
        : "memory","%rax","%rbx","%rcx","%rdx","%r8"
    );


}

void imageprocess::oscurecerLUT(uchar * tablaLUT, uchar gB)
{
    uchar *dirLUT = tablaLUT;
//    for(int g=0; g<=gB; g++)
//        *(dirLUT+g) = 0;

    //std::cout << gB<< std::endl;


    asm volatile(
        "\n\t"
                "mov %0,%%r8\n\t"
                "movzbq %1,%%r9\n\t"
                "mov $0,%%rcx\n\t"
                "o1:\n\t"
                "cmp %%r9,%%rcx\n\t"
                "jg no1\n\t"

                        "mov %%r8,%%rax\n\t"
                        "add %%rcx,%%rax\n\t"
                        "movb $0,(%%rax)\n\t"

                "inc %%rcx\n\t"
                "jmp o1\n\t"
                "no1:\n\t"


                "mov %%r9,%%rcx\n\t"
                "inc %%rcx\n\t"
                "o2:\n\t"
                "cmp $256,%%rcx\n\t"
                "jge no2\n\t"

                        "mov %%rcx,%%rax\n\t"
                        "subb %%r9b,%%al\n\t"
                        "movw $255,%%bx\n\t"
                        "mulw %%bx\n\t"
                        "mov $255,%%rbx\n\t"
                        "sub %%r9,%%rbx\n\t"
                        "divw %%bx\n\t"
                        "mov %%r8,%%rdx\n\t"
                        "add %%rcx,%%rdx\n\t"
                        "movb %%al,(%%rdx)\n\t"

                "inc %%rcx\n\t"
                "jmp o2\n\t"
                "no2:"


                :
                : "m" (tablaLUT), "m" (gB)
                : "memory","%rax","%rbx","%rcx","%rdx","%r8"
                );

//    for(int g=gB+1; g<256; g++)
//        *(dirLUT+g) = ((g-gB)*255)/(255-gB);

}


void imageprocess::iluminarLUTMejorado(uchar * tablaLUT, uchar gW)
{
    float pi = 3.1416;
    uchar * dirLUT = tablaLUT;
    for(int g=0; g<gW; g++)
        *(dirLUT+g) = gW*sin((pi*g)/(2*gW));

    for(int g=gW; g<256; g++)
        *(dirLUT+g) = g;
//    asm volatile(
//        "\n\t"


//        :
//        : "m" (tablaLUT), "m" (gW)
//        : "memory"
//    );

}



void imageprocess::oscurecerLUTMejorado(uchar * tablaLUT, uchar gB)
{

    float pi = 3.1416;
    uchar * dirLUT = tablaLUT;
    for(int g=0; g<gB; g++)
    *(dirLUT+g) = gB*(1-cos((pi*g)/(2*gB)));

    for(int g=gB; g<256; g++)
    *(dirLUT+g) = g;
//    asm volatile(
//        "\n\t"


//        :
//        : "m" (tablaLUT), "m" (gB)
//        : "memory"
//    );

}



void imageprocess::aplicarTablaLUT(uchar * tablaLUT, uchar * imgO, uchar * imgD)
{

    /*
     * tabla lut
     *
     * dirlut rax
     * dirOrig rbx
     * dirDest rdx
     *
     * gOrig rsi
     * gDest rdi
     *
     * p rcx
     * */


    asm volatile(
                "\n\t"

                "mov %0,%%rax\n\t"
                "mov %1,%%rbx\n\t"
                "mov %2,%%rdx\n\t"


                "movq $0,%%rcx\n\t"
                "tabla_for:\n\t"
                "cmp $307199, %%rcx\n\t"
                "jg fin_tabla\n\t"

                        "movb (%%rbx),%%sil\n\t" //gOrig = *dirOrig;
                        "movzbq %%sil,%%r8\n\t"
                        "add %%rax,%%r8\n\t"
                        "mov (%%r8),%%dil\n\t" //gDest = *(dirLUT + gOrig);
                        "mov %%dil,(%%rdx)\n\t" //*dirDest = gDest;
                        "inc %%rbx\n\t"
                        "inc %%rdx\n\t"


                "inc %%rcx\n\t"
                "jmp tabla_for\n\t"
                "fin_tabla:\n\t"



                :
                :"m"(tablaLUT),"m"(imgO),"m"(imgD)
                :"memory","%rax","%rbx","%rcx","%rdx","%rsi","%rdi","%r8"
                );


}

