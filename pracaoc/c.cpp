/***************************************************************************
 *   Copyright (C) 2018 by pilar   *
 *   pilarb@unex.es   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "imageprocess.h"


void imageprocess::rotar(uchar * imgO, uchar * imgD, float angle)
{
    short cw;
    int sin1000, cos1000;

    sin1000 = sin(angle)*1000.;
    cos1000 = cos(angle)*1000.;

    uchar * dirOrig = imgO;
    uchar * dirDest = imgD;
    int fO, cO, indiceO;

    for(int fD=0; fD<800; fD++)
    {
        for(int cD=0; cD<800; cD++)
        {
            fO = (sin1000*(cD-400) + cos1000*(fD-400))/1000;
            fO = fO + 240;
            cO = (cos1000*(cD-400) - sin1000*(fD-400))/1000;
            cO = cO + 320;
            if(fO>=0 && fO<480 && cO>=0 && cO <640)
            {
                indiceO = fO*640+cO;
                *dirDest=*(dirOrig+indiceO);
            }
            else
                *dirDest=0;
            dirDest++;
        }
}

//    asm volatile(
//        "fstcw %3\n\t"
//        "fwait\n\t"
//        "mov %3, %%ax\n\t"
//        "and $0xf3ff, %%ax\n\t"
//        "or $0x0c00, %%ax\n\t"
//        "push %%rax\n\t"
//        "fldcw (%%rsp)\n\t"
//        "pop %%rax\n\t"


//        //Insertar aquí el código del procedimiento


//        "fldcw %3\n\t"

//        :
//        : "m" (imgO), "m" (imgD), "m" (angle), "m" (cw), "m" (sin1000), "m" (cos1000)
//        : "memory"

//    );


}

void imageprocess::zoom(uchar * imgO, uchar * imgD, float s, int dx, int dy)
{
    short cw;
    int sInt;
    int ampliar;


    if(s>=1)
    {
        sInt = s;
        ampliar = 1;
    }
    else
    {
        sInt = rint(1./s);
        ampliar = 0;
    }

    uchar * dirOrig = imgO;
    uchar * dirDest = imgD;
    int fO,cO,indiceO;
    for(int fD=0; fD<480; fD++)
    {
        if (ampliar)
            fO = (fD+dy)/sInt;
        else
            fO = (fD+dy)*sInt;
        for(int cD=0; cD<640; cD++)
        {
            if(ampliar)
                cO = (cD+dx)/sInt;
            else
                cO = (cD+dx)*sInt;
            if(fO>=0 && fO<800 && cO>=0 && cO <800)
            {
                indiceO = fO*800+cO;
                *dirDest=*(dirOrig+indiceO);
            }
            else
                *dirDest=0;
            dirDest++;
        }
    }









//    asm volatile(
//        "fstcw %5\n\t"
//        "fwait\n\t"
//        "mov %5, %%ax\n\t"
//        "and $0xf3ff, %%ax\n\t"
//        "or $0x0c00, %%ax\n\t"
//        "push %%rax\n\t"
//        "fldcw (%%rsp)\n\t"
//        "pop %%rax\n\t"

//        //Insertar aquí el código del procedimiento


//        "fldcw %5\n\t"

//        :
//        : "m" (imgO), "m" (imgD), "m" (s), "m" (dx), "m" (dy), "m" (cw), "m" (sInt), "m" (ampliar)
//        : "memory"

//    );


}



void imageprocess::volteoHorizontal(uchar * imgO, uchar * imgD)
{

//    uchar *dirOrig = imgO;
//    uchar *dirDest = imgD;
//    dirOrig = dirOrig + 639;
//    for(int f=0; f<480; f++)
//    {
//        for(int c=0; c<640; c++)
//        {
//            *dirDest=*dirOrig;
//            dirOrig--;
//            dirDest++;
//        }
//        dirOrig=dirOrig+1280;
//    }
    asm volatile(
        "\n\t"

                "mov %0,%%r8\n\t"//imgOrg
                "mov %1,%%r9\n\t"//imgDst
                "add $639,%%r8\n\t"

                "mov $0,%%rcx\n\t"
                "altVertical:\n\t"

                        "push %%rcx\n\t"
                        "mov $0,%%rcx\n\t"
                        "anchVertical:\n\t"

                                "movb (%%r8),%%al\n\t"
                                "movb %%al,(%%r9)\n\t"
                                "dec %%r8\n\t"
                                "inc %%r9\n\t"

                        "inc %%rcx\n\t"
                        "cmp $640,%%rcx\n\t"
                        "jl anchVertical\n\t"

                        "pop %%rcx\n\t"
                        "add $1280,%%r8\n\t"

                "inc %%rcx\n\t"
                "cmp $480,%%rcx\n\t"
                "jl altVertical\n\t"

        :
        : "m" (imgO),	"m" (imgD)
        : "memory","%rcx","%rax","%r8","%r9"
    );


//    asm volatile(
//        "\n\t"


//        :
//        : "m" (imgO),	"m" (imgD)
//        : "memory"
//    );

}

void imageprocess::volteoVertical(uchar * imgO, uchar * imgD)
{
/*
    uchar *dirOrig = imgO;
    uchar *dirDest = imgD;
    dirOrig = dirOrig + 479*640;
    for(int f=0; f<480; f++)
    {
        for(int c=0; c<640; c++)
        {
            *dirDest=*dirOrig;
            dirOrig++;
            dirDest++;
        }
        dirOrig=dirOrig-1280;
    }

*/


    asm volatile(
        "\n\t"

                "mov %0,%%r8\n\t"//imgOrg
                "mov %1,%%r9\n\t"//imgDst
                "add $306560,%%r8\n\t"

                "mov $0,%%rcx\n\t"
                "altHorizontal:\n\t"

                        "push %%rcx\n\t"
                        "mov $0,%%rcx\n\t"
                        "anchHorizontal:\n\t"

                                "movb (%%r8),%%al\n\t"
                                "movb %%al,(%%r9)\n\t"
                                "inc %%r8\n\t"
                                "inc %%r9\n\t"

                        "inc %%rcx\n\t"
                        "cmp $640,%%rcx\n\t"
                        "jl anchHorizontal\n\t"

                        "pop %%rcx\n\t"
                        "sub $1280,%%r8\n\t"

                "inc %%rcx\n\t"
                "cmp $480,%%rcx\n\t"
                "jl altHorizontal\n\t"

        :
        : "m" (imgO),	"m" (imgD)
        : "memory","%rcx","%rax","%r8","%r9"
    );

}


void imageprocess::iluminarLUT(uchar * tablaLUT, uchar gW)
{
    /*
    uchar * dirLUT = tablaLUT;
    for(int g=0; g<gW; g++)
        *(dirLUT+g) = (g*255)/gW;
    for(int g=gW; g<256; g++)
        *(dirLUT+g) = 255;
*/

    /*
     * dirLUT -16 r8
     * gw     -28 rbx
     * g      -4  rcx
     *
     */



    asm volatile(
                "\n\t"
                "mov %0,%%r8\n\t"
                "movzbq %1,%%rbx\n\t"
                "dec %%rbx\n\t"
                "mov $0, %%rcx\n\t"

                "i1:\n\t"
                        "movb %%cl,%%dl\n\t"
                        "salw $8,%%dx\n\t"
                        "subb %%cl,%%dl\n\t"
                        "movzbq %%dl,%%rax\n\t"
                        "divb %%bl\n\t"

                        "mov %%rcx,%%rdx\n\t"
                        "add %%r8,%%rdx\n\t"

                        "movb %%al,(%%rdx)\n\t"

                "inc %%rcx\n\t"
                "cmp %%rcx,%%rbx\n\t"
                "jl i1\n\t"


                "mov %%rbx, %%rcx\n\t"

                "i2:\n\t"

                    "mov %%r8,%%rax\n\t"
                    "add %%rcx,%%rax\n\t"
                    "movb $255,(%%rax)\n\t"

                "inc %%rcx\n\t"
                "cmp $256,%%rcx\n\t"
                "jl i2\n\t"





        :
        : "m" (tablaLUT), "m" (gW)
        : "memory","%rax","%rbx","%rcx","%rdx","%r8"
    );


}

void imageprocess::oscurecerLUT(uchar * tablaLUT, uchar gB)
{
    uchar *dirLUT = tablaLUT;
//    for(int g=0; g<=gB; g++)
//        *(dirLUT+g) = 0;




    asm volatile(
        "\n\t"
                "mov %0,%%r8\n\t"
                "mov $0,%%rcx\n\t"
                "o1:\n\t"
                        "mov %%r8,%%rax\n\t"
                        "add %%rcx,%%rax\n\t"
                        "movb $0,(%%rax)\n\t"

                "incb %%cl\n\t"
                "cmpb %1,%%cl\n\t"
                "jle o1\n\t"

                "movzbq %1,%%rcx\n\t"
                "inc %%rcx\n\t"
                "o2:\n\t"
                        "mov %%rcx,%%rax\n\t"
                        "subb %1,%%al\n\t"
                        "movb $255,%%bl\n\t"
                        "mulb %%bl\n\t"
                        "mov $255,%%rbx\n\t"
                        "sub %1,%%rbx\n\t"
                        "divb %%bl\n\t"
                        "mov %%r8,%%rdx\n\t"
                        "add %%rcx,%%rdx\n\t"
                        "movb %%al,(%%rdx)\n\t"

                "incb %%cl\n\t"
                "cmpb $256,%%cl\n\t"
                "jl o2\n\t"


                :
                : "m" (tablaLUT), "m" (gB)
                : "memory","%rax","%rbx","%rcx","%rdx","%r8"
                );

//    for(int g=gB+1; g<256; g++)
//        *(dirLUT+g) = ((g-gB)*255)/(255-gB);

}


void imageprocess::iluminarLUTMejorado(uchar * tablaLUT, uchar gW)
{
    float pi = 3.1416;
    uchar * dirLUT = tablaLUT;
    for(int g=0; g<gW; g++)
        *(dirLUT+g) = gW*sin((pi*g)/(2*gW));

    for(int g=gW; g<256; g++)
        *(dirLUT+g) = g;
//    asm volatile(
//        "\n\t"


//        :
//        : "m" (tablaLUT), "m" (gW)
//        : "memory"
//    );

}



void imageprocess::oscurecerLUTMejorado(uchar * tablaLUT, uchar gB)
{

    float pi = 3.1416;
    uchar * dirLUT = tablaLUT;
    for(int g=0; g<gB; g++)
    *(dirLUT+g) = gB*(1-cos((pi*g)/(2*gB)));

    for(int g=gB; g<256; g++)
    *(dirLUT+g) = g;
//    asm volatile(
//        "\n\t"


//        :
//        : "m" (tablaLUT), "m" (gB)
//        : "memory"
//    );

}



void imageprocess::aplicarTablaLUT(uchar * tablaLUT, uchar * imgO, uchar * imgD)
{

    /*
     * tabla lut
     *
     * dirlut rax
     * dirOrig rbx
     * dirDest rdx
     *
     * gOrig rsi
     * gDest rdi
     *
     * p rcx
     * */


    asm volatile(
                "\n\t"

                "mov %0,%%rax\n\t"
                "mov %1,%%rbx\n\t"
                "mov %2,%%rdx\n\t"


                "movq $0,%%rcx\n\t"
                "tabla_for:\n\t"
                "cmp $307199, %%rcx\n\t"
                "jg fin_tabla\n\t"

                        "movb (%%rbx),%%sil\n\t" //gOrig = *dirOrig;
                        "movzbq %%sil,%%r8\n\t"
                        "add %%rax,%%r8\n\t"
                        "mov (%%r8),%%dil\n\t" //gDest = *(dirLUT + gOrig);
                        "mov %%dil,(%%rdx)\n\t" //*dirDest = gDest;
                        "inc %%rbx\n\t"
                        "inc %%rdx\n\t"


                "inc %%rcx\n\t"
                "jmp tabla_for\n\t"
                "fin_tabla:\n\t"



                :
                :"m"(tablaLUT),"m"(imgO),"m"(imgD)
                :"memory","%rax","%rbx","%rcx","%rdx","%rsi","%rdi","%r8"
                );


}

